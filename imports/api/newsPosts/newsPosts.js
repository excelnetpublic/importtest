import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

class NewsPostsCollection extends Mongo.Collection {
  insert(list, callback) {
    // Allows for insert logic / massaging here
    return super.insert(list, callback);
  }
  remove(selector, callback) {
    // Useful here to potentiall remove relationship items
    return super.remove(selector, callback);
  }
}

export const NewsPosts = new NewsPostsCollection('NewsPosts');

// Schema definition and attachment for enforcement
NewsPosts.schema = new SimpleSchema({
  title: {
    type: String,
    label: 'Title',
    max: 100,
  },
  posting: {
    type: String,
    label: 'Posting',
  },
  created: {
    type: Date,
    label: 'Created at',
    autoValue: () => Date,
  },
  postUntil: {
    type: Date,
    label: 'Post until',
    optional: true,
  },
  userId: {
    type: String,
    label: 'User ID',
    regEx: SimpleSchema.RegEx.Id,
    autoValue: () => this.userId,
  },
  url: {
    type: String,
    label: 'Linked URL',
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
});
NewsPosts.attachSchema(NewsPosts.schema);

// This should be used in publish to client so that secret fields not leaked
NewsPosts.publicFields = {
  title: 1,
  posting: 1,
  created: 1,
  postUntil: 1,
  userId: 1,
  url: 1,
};

NewsPosts.helpers({

});
