import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';

import { NewsPosts } from '../newsPosts.js';

Meteor.publish('newsPosts.latest', function newsPostsLatest() {
  return NewsPosts.find({
    $or: [{ postUntil: { $exists: false } }, { postUntil: { $lte: Date() } }],
  }, {
    fields: NewsPosts.publicFields,
    limit: 4,
    sort: { created: -1 },
  });
});

Meteor.publish('newsPosts.private', function newsPostsPrivate() {
  if (!Roles.userIsInRole(this.userId, ['newsPosts'])) {
    return this.ready();
  }

  return NewsPosts.find({

  }, {
    fields: NewsPosts.publicFields,
  });
});
