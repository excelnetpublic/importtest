import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { AccountsTemplates } from 'meteor/useraccounts:core';


FlowRouter.route('/', {
  name: 'App.Home',
  action() {
    BlazeLayout.render('AppMain', { main: 'App_Home' });
  },
});

// the App_notFound template is used for unknown routes and missing lists
FlowRouter.notFound = {
  action() {
    BlazeLayout.render('AppMain', { main: 'App_NotFound' });
  },
};

// Routing for the Accounts functions
AccountsTemplates.configureRoute('signIn', {
  name: 'Accounts.Login',
  path: '/login',
});

AccountsTemplates.configureRoute('signUp', {
  name: 'Accounts.join',
  path: '/join',
});

AccountsTemplates.configureRoute('forgotPwd');

AccountsTemplates.configureRoute('resetPwd', {
  name: 'Accounts.resetPwd',
  path: '/reset-password',
});
