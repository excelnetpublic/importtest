import { AccountsTemplates } from 'meteor/useraccounts:core';

AccountsTemplates.configure({
  showForgotPasswordLink: true,
  texts: {
    errors: {
      loginForbidden: 'Incorrect username or password',
      pwdMismatch: 'Passwords don\'t match',
    },
    title: {
      signIn: 'Customer or Staff Login',
      signUp: 'Register for Web Site Access',
    },
  },
  defaultTemplate: 'Auth_page',
  defaultLayout: 'AppMain',
  defaultContentRegion: 'main',
  defaultLayoutRegions: {},
});
