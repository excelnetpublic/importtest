import { Accounts } from 'meteor/accounts-base';


Accounts.emailTemplates.siteName = 'My App';
Accounts.emailTemplates.from = 'My Admin Mail <admin@excel.net>';

Accounts.emailTemplates.resetPassword = {
  subject() {
    return 'Reset your password on My App';
  },
  text(user, url) {
    return `Hello!

Click the link below to reset your password on My App.

${url}

If you didn't request this email, please ignore it.

Thanks,
My App Admin
`;
  },
};
