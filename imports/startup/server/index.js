// Load the site with sample / starting data if no data to start
import './fixtures.js';

// Configure Accounts package
import './accounts-configure.js';

// Security setup and rate limiting
import './security.js';

// Register the API's for collections, methods and publications
import './register-api.js';
